Ti.include("settings.js");

var win = Ti.UI.currentWindow;

var personerTableView = Ti.UI.createTableView({
	top: "50dp",
	bottom: "0dp"
}); 

var currentObject;
personerTableView.addEventListener("click", function(e) {
	// Spara objectId för rad som klickats på
	currentObject = e.rowData.objectId;
	
	// Öppna galleriet för att väja bild
	Ti.Media.openPhotoGallery({
		success: function(e) {
			blob = e.media;
			// Skicka med bilden till uplaodPhoto
			uploadPhoto(blob);
		},
		error: function() {
			
		},
		cancel: function() {
			
		}
	});
});

function uploadPhoto(blob)
{
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function(e) {
		// Exempel på svar: http://files.parse.com/ac16fcb0-4d0c-49bd-8ae0-fb2c48499524/4cec4afe-8bf9-4760-815b-29f07628e58e-bild               
		imageName = this.getResponseHeader("Location");
		imageName = imageName.substring(imageName.lastIndexOf('/')+1, imageName.length);
		// imageName är nu bild-namn som parse kan använda
		// Skicka till setPhoto
		setPhoto(imageName);
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('POST', "https://api.parse.com/1/files/bild");
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	c.setRequestHeader("Content-Type", blob.mimeType);
	
	c.send(blob);
}

function setPhoto(parseImage)
{
	// Spara bild-namn med rad 
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		getData();
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('PUT', "https://api.parse.com/1/classes/personer/"+currentObject);
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	c.setRequestHeader("Content-Type", "application/json");
	
	var params = {
		pic: {
			name: parseImage,
			__type: "File"
		}
	};	
	
	c.send(JSON.stringify(params));
	
}

win.add(personerTableView);

var reloadButton = Ti.UI.createButton({
	title: "Ladda om",
	top: "5dp",
	left: "5dp"
});
reloadButton.addEventListener("click", function(e) {
	getData();
});
win.add(reloadButton);

var personerTableData = [];
function makeRow(objectId, personName, personImage)
{
	row = Ti.UI.createTableViewRow({
		height: "50dp",
		objectId: objectId
	});
	
	personLabel = Ti.UI.createLabel({
		text: personName,
		left: "5dp"
	});	
	row.add(personLabel);

	personImage = Ti.UI.createImageView({
		image: personImage,
		height: "50dp",
		width: "auto", 
		right: "5dp"
	});
	row.add(personImage);
	
	personerTableData.push(row);
}

function getData()
{
	personerTableData = [];
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		json = JSON.parse(this.responseText);
		for(i = 0;i < json.results.length;i++)
		{
			picUrl = "";
			if(json.results[i].pic !== undefined)
			{
				picUrl = json.results[i].pic.url;
			}
			makeRow(json.results[i].objectId, json.results[i].name, picUrl);
		}
		
		personerTableView.setData(personerTableData);
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('GET', 'https://api.parse.com/1/classes/personer/');
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	
	c.send();

}

getData();