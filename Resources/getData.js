Ti.include("settings.js");

var win = Ti.UI.currentWindow;

var personerTableView = Ti.UI.createTableView({
	top: "50dp",
	bottom: "0dp",
	editable: true
}); 

var x= 'funkar';
// Radera event på tableview
personerTableView.addEventListener("delete", function(e) {
	// Gör delete mot ditt object i din class
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		alert("Raderad");
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('DELETE', "https://api.parse.com/1/classes/Kontakter/"+e.rowData.objectId);
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	
	c.send();
});

personerTableView.addEventListener("click", function(e) {
	

	// Öppna nytt fönster och skicka med objectId från raden
	// moreInfoWin = Ti.UI.createWindow({
		// url: "moreInfo.js",
		// objectId: e.rowData.objectId,
		// backgroundColor: "#ffffff"
	// });
	// Ti.UI.currentTab.open(moreInfoWin);
	
	// Kommentera bort ovan och ta fram nedan för att ändra så klick på raden ändrar bgColor till yellow
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		getData();
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('PUT', "https://api.parse.com/1/classes/Kontakter/"+e.rowData.objectId);
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	
	if (e.row.backgroundColor== '#E6A959'){

	var params = {
		bgColor: "#fff",
		whoClicked: x
	};	
	
	c.send(JSON.stringify(params));
	
	}
	
	else if (e.row.backgroundColor== '#fff'){

	var params = {
		bgColor: "#E6A959",
		whoClicked:'Ove'
	
	};	
	
	c.send(JSON.stringify(params));
	
	}
	
});


win.add(personerTableView);

var reloadButton = Ti.UI.createButton({
	title: "Ladda om",
	top: "5dp",
	left: "5dp"
});
reloadButton.addEventListener("click", function(e) {
	getData();
});
win.add(reloadButton);

var personerTableData = [];
function makeRow(objectId, cellphone, name, bgColor, whoClicked)
{
	row = Ti.UI.createTableViewRow({
		height: "50dp",
		objectId: objectId,
		backgroundColor: bgColor,
		
		
	});
	
	
		clickedLabel = Ti.UI.createLabel({
			font:{
				fontSize:8
			},
		text: whoClicked,
		left: "150dp"
	});	
	row.add(clickedLabel);
	
	
	personLabel = Ti.UI.createLabel({
	
		text: name,
		left: "5dp"
	});	
	row.add(personLabel);

	phoneLabel = Ti.UI.createLabel({
		text: cellphone,
		right: "5dp"
	});	
	row.add(phoneLabel);
	
	personerTableData.push(row);
}

// Funktion för att hämta data
function getData()
{
	personerTableData = [];
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		json = JSON.parse(this.responseText);
		
		// Alla hämtade rader finns i json.results
		for(i = 0;i < json.results.length;i++)
		{
			makeRow(json.results[i].objectId, json.results[i].cellphone, json.results[i].name, json.results[i].bgColor,json.results[i].whoClicked);
		}
		
		personerTableView.setData(personerTableData);
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	// Bortkommenterad rad för att hämta rader gär prylar är större än 200
	//c.open('GET', 'https://api.parse.com/1/classes/personer/?where={"prylar":{"$gte":200}}');
	// Hämta rader sorterade efter name
	c.open('GET', 'https://api.parse.com/1/classes/Kontakter/?order=name');
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	
	c.send();

}

getData();