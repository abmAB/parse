Ti.include("settings.js");

var win = Ti.UI.currentWindow;

var nameField = Ti.UI.createTextField({
	top: "20dp",
	hintText: "Namn",
	left: "10dp",
	right: "10dp"
});

win.add(nameField);

var phoneField = Ti.UI.createTextField({
	top: "50dp",
	hintText: "Telefon", 
	left: "10dp",
	right: "10dp"
});

win.add(phoneField);

var saveButton = Ti.UI.createButton({
	title: "Spara",
	top: "100dp"
});
saveButton.addEventListener("click", function(e) {
	
	
	// Lägga till ny rad i parse
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		alert("Sparat");
	};
	c.onerror = function(e) {
		alert(e);
	};
	
	c.open('POST', "https://api.parse.com/1/classes/Kontakter/");
	
	c.setRequestHeader("X-Parse-Application-Id", parseId);
	c.setRequestHeader("X-Parse-REST-API-Key", parseKey);
	c.setRequestHeader("Content-Type", "application/json");
	
	var params = {
		name: nameField.value,
		cellphone: phoneField.value,
		bgColor:'#E6A959'
	};
	
	
	c.send(JSON.stringify(params));

});
win.add(saveButton);

