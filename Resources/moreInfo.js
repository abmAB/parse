Ti.include("settings.js");

var win = Ti.UI.currentWindow;

// Medskickat objectId finns i win.objectId

var createdLabel = Ti.UI.createLabel({
	text: "",
	bottom: "5dp",
	left: "10dp",
	right: "10dp",
	height: "auto"
});

win.add(createdLabel);

var moreInfoLabel = Ti.UI.createLabel({
	text: "",
	top: "20dp",
	left: "10dp",
	right: "10dp",
	height: "auto"
});

win.add(moreInfoLabel);

var personImage = Ti.UI.createImageView({
	top: "150dp",
	width: "100dp",
	height: "auto"
});
win.add(personImage);

c = Titanium.Network.createHTTPClient();
c.setTimeout(25000);
c.onload = function() {
	json = JSON.parse(this.responseText);
	
	moreInfoLabel.text = json.results[0].description;

	if(json.results[0].pic !== undefined)
	{
		personImage.image = json.results[0].pic.url;
	}
	
	createdLabel.text = json.results[0].createdAt;
};
c.onerror = function(e) {
	alert(e);
};

// Hämta rader där objectId är medskickad värde
c.open('GET', 'https://api.parse.com/1/classes/Kontakter/?where={"objectId":"'+win.objectId+'"}');

c.setRequestHeader("X-Parse-Application-Id", parseId);
c.setRequestHeader("X-Parse-REST-API-Key", parseKey);

c.send();
	

